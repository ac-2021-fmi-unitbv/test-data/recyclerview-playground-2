package com.example.applab4.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.applab4.R;
import com.example.applab4.models.Movie;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MovieViewHolder> {
    ArrayList<Movie> movies;

    public MyAdapter(ArrayList<Movie> movies)
    {
        this.movies=movies;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_movie, parent, false);
        MovieViewHolder movieViewHolder = new MovieViewHolder(view);

        Log.e("onCreateViewHolder","Here");
        return movieViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        Movie movie=movies.get(position);
        holder.bind(movie);

        Log.e("onBindViewHolder","Position = "+position);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    class MovieViewHolder extends RecyclerView.ViewHolder{
        private TextView title;
        private TextView description;

        public MovieViewHolder(View view)
        {
            super(view);
            title=view.findViewById(R.id.tv_title);
            description=view.findViewById(R.id.tv_description);
        }

        public void bind(Movie movie)
        {
            title.setText(movie.getTitle());
            description.setText(movie.getDescription());
        }
    }
}

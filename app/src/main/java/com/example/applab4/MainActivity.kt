package com.example.applab4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.applab4.adapters.MyAdapter
import com.example.applab4.models.Movie

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecyclerView()
    }

    fun setupRecyclerView(){
        val recyclerView : RecyclerView = findViewById(R.id.rv_movies_list)

        val linearLayoutManager : LinearLayoutManager = LinearLayoutManager(this)

        val moviesList : ArrayList<Movie> = ArrayList<Movie>()
        moviesList.add(Movie("Star Wars 0", "Master Jedi"))
        moviesList.add(Movie("Star Wars 1", "Master Jedi"))
        moviesList.add(Movie("Star Wars 2", "Master Jedi"))
        moviesList.add(Movie("Star Wars 3", "Master Jedi"))
        moviesList.add(Movie("Star Wars 4", "Master Jedi"))
        moviesList.add(Movie("Star Wars 5","Master Jedi"))
        moviesList.add(Movie("Star Wars 6", "Master Jedi"))
        moviesList.add(Movie("Star Wars 7", "Master Jedi"))
        moviesList.add(Movie("Star Wars 8", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))
        moviesList.add(Movie("Star Wars 9", "Master Jedi"))

        val myAdapter : MyAdapter = MyAdapter(moviesList)

        recyclerView.layoutManager=linearLayoutManager
        recyclerView.adapter=myAdapter
    }
}